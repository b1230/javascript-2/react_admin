import { Outlet } from 'react-router-dom'
import config from '../../configs/default'

export default function Login ({ logo, title }) {
    return (
        <div className="flex justify-center">
            <div className="w-1/2 flex flex-col justify-center align-middle h-screen bg-[#0067F4]">
                <div className="flex justify-center h-60"><img src={config.logo} alt="dasf"/></div>
                <div className="text-4xl">{config.welcome}</div>
            </div>
            <div className="w-1/2 h-screen bg-[#E5E5E5]"><Outlet /></div>
        
        </div>
    )
}